#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->neededDevelop_lstwdgt->setHidden(true);
    str<<"All License"<<"All Themselves"<<"FixedBudget"<<"ThinkIfIsWorth";
    ui->label_4->setText("Technology\ttime\tmoney");
    const int n=20;
    const int m=10;
    technology *t[n];
    for (int i=0;i<n;i++)
    {
        t[i]=new technology;
        allTechnology.push_back(t[i]);
    }
    for (int i=0;i<n;i++)
    {
        for (int j=i+1;j<n;j++)
        {
            if (j%(i+1)==qrand()%10||j%(i+1)==qrand()%10||j%(i+1)==qrand()%10||j%(i+1)==qrand()%10||j%(i+1)==qrand()%10)
            {
                t[i]->addRequirementTechnology(t[j]);
            }
        }


    }


    for (int i=0;i<m;i++)
    {
        company *tmp1;
        switch (qrand()%4)
        {
        case 0: tmp1=new company_AllLicense; break;
        case 1: tmp1=new company_AllThemSelves; break;
        case 2: tmp1=new company_FixedBudget; break;
        case 3: tmp1=new company_ThinkIfIsWorthDevelop; break;
        default: exit(12);
        }

        for (int j=0;j<n;j++)
        {
            if (j%(i+1)==qrand()%10||j%(i+1)==qrand()%10||j%(i+1)==qrand()%10||j%(i+1)==qrand()%10)
                tmp1->addWantedTechnology(t[j]);

        }
        tmp1->start();compainies.push_back(tmp1);
        ui->companies_lstwdgt->addItem(tmp1->Name()+'\t'+str[tmp1->type]);
    }
    ui->companies_lstwdgt->setCurrentRow(0);



    rewrite_company();
    switch (compainies[ui->companies_lstwdgt->currentRow()]->type)
    {
    case 0: tmp=new company_AllLicense; break;
    case 1: tmp=new company_AllThemSelves; break;
    case 2: tmp=new company_FixedBudget; break;
    case 3: tmp=new company_ThinkIfIsWorthDevelop; break;
    default: exit(12);
    }
    changedCompany();

    connect(ui->companies_lstwdgt, SIGNAL(currentRowChanged(int )), this, SLOT(changedCompany()));
    connect(ui->selectedTechnologies_lstwdgt, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(item_changed(QListWidgetItem*)));



}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_next_day_clicked()
{
    ui->current_day_lbl->setText("current date "+QString::number(++date));
    int size=compainies.size();
    for (int i=0;i<size;i++)
        compainies[i]->next_moment_time();
    rewrite_company();
}

void MainWindow::changedCompany()//переписати тут
{
    company *a=compainies[ui->companies_lstwdgt->currentRow()];

    //rewrite
    {
        flag=true;
        ui->selectedTechnologies_lstwdgt->clear();
        int size=a->ListWantedTechnology.size();
        for (int i=0;i<size;i++)
        {
            ui->selectedTechnologies_lstwdgt->addItem(a->ListWantedTechnology[i]->name());
            QListWidgetItem* item=ui->selectedTechnologies_lstwdgt->item(i);
            item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
            if (a->technologies_selected[i])
                item->setCheckState(Qt::Checked);
            else
                item->setCheckState(Qt::Unchecked);
        }
        flag=false;

    }

    current_ittems_changet();
    rewrite_company();
}

void MainWindow::rewrite_company()
{
    //int cur_row=ui->companies_lstwdgt->currentRow();

    company *a=compainies[ui->companies_lstwdgt->currentRow()];
    //list wanted technology
    {
        int size=a->ListWantedTechnology.size();
        ui->wantedDevelop_lstwdgt->clear();

        for (int i=0;i<size;i++)
        {
            technology* tech=a->ListWantedTechnology[i];
            ui->wantedDevelop_lstwdgt->addItem(tech->name()+'\t'+QString::number(tech->TimeIsDeveloping())+'\t'+QString::number(tech->MoneyIsDeveloping()));

        }

    }

    //list needed technology
    {
        int size=a->listNeededToBeAdded.size();
        ui->neededDevelop_lstwdgt->clear();
        for (int i=0;i<size;i++)
        {
            technology* tech=a->listNeededToBeAdded[i];
            ui->neededDevelop_lstwdgt->addItem(tech->name());

        }
    }

    //list story technology
    {
        int size=a->ListStoryTechnology.size();
        ui->storyTechnologe_lstwdgt->clear();
        for (int i=0;i<size;i++)
        {
            technologyStory tech=a->ListStoryTechnology[i];
            if (tech.is_end)
                ui->storyTechnologe_lstwdgt->addItem(tech.developedTechnology->name()+'\t'+QString::number(tech.developedTechnology->TimeIsDeveloping())+'\t'+QString::number(tech.start)+'\t'+QString::number(tech.end));
            else
                ui->storyTechnologe_lstwdgt->addItem(tech.developedTechnology->name()+'\t'+QString::number(tech.developedTechnology->TimeIsDeveloping())+'\t'+QString::number(tech.start)+"\tdeveloping");
        }

    }

    //list licensed technology
    {
        int size=a->LicensedTechnologies.size();
        ui->licensed_lstwdgt->clear();
        for (int i=0;i<size;i++)
        {
            technology* tech=a->LicensedTechnologies[i];
            ui->licensed_lstwdgt->addItem(tech->name()+'\t'+'\t'+QString::number(tech->LicensingPrice()));

        }
    }

    ui->spendMoney_lbl->setText("Money spend now"+QString::number(a->money));



}

void MainWindow::current_ittems_changet()
{
    delete tmp;

    company *a=compainies[ui->companies_lstwdgt->currentRow()];
    switch (a->type)
    {
    case 0: tmp=new company_AllLicense; break;
    case 1: tmp=new company_AllThemSelves; break;
    case 2: tmp=new company_FixedBudget; break;
    case 3: tmp=new company_ThinkIfIsWorthDevelop; break;
    default: exit(12);
    }

    int size=a->ListWantedTechnology.size();
    for (int i=0;i<size;i++)
    {
        if (a->technologies_selected[i])
            tmp->addWantedTechnology(a->ListWantedTechnology[i]);
    }


    tmp->start();


    while (!tmp->EndDevelop)
    {
        tmp->next_moment_time();
    }


    ui->money_will_spend->setText("money will spend "+QString::number(tmp->money));
    ui->time_will_spend->setText("time will spend "+QString::number(tmp->currentDate_int));
}

void MainWindow::item_changed(QListWidgetItem *item)
{
    if (flag) return;
    company *a=compainies[ui->companies_lstwdgt->currentRow()];
    int size=a->ListWantedTechnology.size();
    for (int i=0;i<size;i++)
    {
        if (ui->selectedTechnologies_lstwdgt->item(i)==item)
        {
            a->technologies_selected[i]=!a->technologies_selected[i];
            rewrite_company();
            current_ittems_changet();
            return;
        }
    }


}
