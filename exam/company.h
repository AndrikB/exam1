#ifndef COMPANY_H
#define COMPANY_H
#include "technology.h"
#include <QDebug>



class company
{
public:


    company(QString Name="Company"+QString::number(number++)+' ');
    virtual~company(){}

    void addWantedTechnology(technology* item);
    void addStoryTechnology(technology* item, int Start, int End);
    void addStoryTechnology(technologyStory item);

    QString Name(){return name;}
    void next_moment_time();

    virtual void start()=0;

    QVector<technology*> ListWantedTechnology;
    QVector<technologyStory> ListStoryTechnology;
    QVector<technology*> LicensedTechnologies;

    QVector<technology*> listNeededToBeAdded;

    int money=0;
    bool EndDevelop=false;

    int type;//for subcompany
    int currentDate_int=0;//++
    bool *technologies_selected;
protected:

    int days_to_the_end_of_technology_development;
    bool isBeingDevelopen=false;



    void addNewTechnology(technology*);
    void endTechnology();
    bool canAddNewTechnology(technology* tech);
    bool isInStory_orLicensed(technology* tech);
    void addNewLicense(technology* tech);
    void create_ListNeededToBeAdded();

private:
    QString name;
    void sortWantedTechnology();
    QVector<technology*> allRequirementTechnology(technology*);

    static int number;

    virtual void choseNewTechnology()=0;
};



#endif // COMPANY_H
