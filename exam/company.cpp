#include "company.h"


int company::number = 0;

company::company(QString Name)
{
    this->name=Name;
}

void company::addWantedTechnology(technology *item)
{
    ListWantedTechnology.push_back(item);
    sortWantedTechnology();
}

void company::addStoryTechnology(technology *item, int Start, int End)
{
    technologyStory TS;
    TS.developedTechnology=item;
    TS.start=Start;
    TS.end=End;
    addStoryTechnology(TS);
}

void company::addStoryTechnology(technologyStory item)
{
    ListStoryTechnology.push_back(item);
}

void company::next_moment_time()
{
    currentDate_int++;
    if (!EndDevelop)
    {

        if (isBeingDevelopen)//if smth is developing
        {
            days_to_the_end_of_technology_development--;

            if (days_to_the_end_of_technology_development==0)//if end technlogy development
            {
                endTechnology();
                if (listNeededToBeAdded.isEmpty())
                {
                    EndDevelop=true;
                }
            }


        }
        else//chose new developing technology
        {
            choseNewTechnology();
            days_to_the_end_of_technology_development=ListStoryTechnology[ListStoryTechnology.size()-1].developedTechnology->TimeIsDeveloping();
            money=money+ListStoryTechnology[ListStoryTechnology.size()-1].developedTechnology->MoneyIsDeveloping();
            isBeingDevelopen=true;

        }
    }

    int size=LicensedTechnologies.size();
    for (int i=0;i<size;i++)
    {
        money=money+LicensedTechnologies[i]->LicensingPrice();
    }
}

void company::addNewTechnology(technology * tech)
{
    technologyStory ts;
    ts.developedTechnology=tech;
    ts.start=currentDate_int;
    ts.is_end=false;
    ListStoryTechnology.push_back(ts);

}

void company::endTechnology()
{
    isBeingDevelopen=false;
    ListStoryTechnology[ListStoryTechnology.size()-1].end=currentDate_int;
    ListStoryTechnology[ListStoryTechnology.size()-1].is_end=true;
}

void company::sortWantedTechnology()
{
    int size=ListWantedTechnology.size();
    for (int i=0;i<size;i++)
    {
        for (int j=i+1;j<size;j++)
        {
            if (ListWantedTechnology[i]->isInRequirementTechnologys(ListWantedTechnology[j]))//if item(j) is in RequirementTechnologe item(i)
            {
                qSwap(ListWantedTechnology[i], ListWantedTechnology[j]);
            }
        }
        
    }
}

QVector<technology *> company::allRequirementTechnology(technology* tech)
{
    QVector<technology*> requirement;
    requirement.push_back(tech);
    int size=tech->listTechnologysRequirement.size();
    for (int i=0;i<size;i++)
    {
        QVector<technology*> tmp=allRequirementTechnology(tech->listTechnologysRequirement[i]);
        int new_size=tmp.size();
        for (int j=new_size-1;j>=0;j--)
            requirement.push_front(tmp[j]);
    }
    return  requirement;
}

bool company::canAddNewTechnology(technology *tech)
{
    int size=tech->listTechnologysRequirement.size();
    for (int i=0;i<size;i++)
    {
        if (!isInStory_orLicensed(tech->listTechnologysRequirement[i]))
            return false;
    }
    return true;
}

bool company::isInStory_orLicensed(technology *tech)
{
    int size=ListStoryTechnology.size();
    for (int i=0;i<size;i++)
    {
        if (ListStoryTechnology[i].developedTechnology==tech)
            return true;
    }
    size=LicensedTechnologies.size();
    for(int i=0;i<size;i++)
    {
        if (LicensedTechnologies[i]==tech)
            return true;
    }


    return false;
}

void company::addNewLicense(technology *tech)
{
    LicensedTechnologies.push_back(tech);

}
void company::create_ListNeededToBeAdded()
{
    int size=ListWantedTechnology.size();
    for (int i=0;i<size;i++)
    {
        if (!canAddNewTechnology(ListWantedTechnology[i]))
        {
            QVector<technology*> tmp=allRequirementTechnology(ListWantedTechnology[i]);
            int new_size=tmp.size();
            for (int i=new_size-1;i>=0;i--)
                listNeededToBeAdded.push_front(tmp[i]);
        }
        else
        {
            listNeededToBeAdded.push_front(ListWantedTechnology[i]);
        }
    }

    size =listNeededToBeAdded.size();
    for(int i=0;i<size;i++)
    {
        for (int j=i+1;j<size;j++)
        {
            if (listNeededToBeAdded[i]==listNeededToBeAdded[j])//find duplicate
            {
                listNeededToBeAdded.erase(listNeededToBeAdded.begin()+j);
                size--;
                j--;
            }
        }
    }
}
