#-------------------------------------------------
#
# Project created by QtCreator 2018-12-10T12:20:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = exam
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    technology.cpp \
    company.cpp \
    companies/company_alllicense.cpp \
    companies/company_allthemselves.cpp \
    companies/company_fixedbudget.cpp \
    companies/company_thinkifisworthdevelop.cpp

HEADERS += \
        mainwindow.h \
    technology.h \
    company.h \
    companies/company_alllicense.h \
    companies/company_allthemselves.h \
    companies/company_fixedbudget.h \
    companies/company_thinkifisworthdevelop.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
