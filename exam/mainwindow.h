#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include "company.h"
#include "companies/company_alllicense.h"
#include "companies/company_allthemselves.h"
#include "companies/company_fixedbudget.h"
#include "companies/company_thinkifisworthdevelop.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_next_day_clicked();

    void changedCompany();
    void rewrite_company();

    void current_ittems_changet();

    void item_changed(QListWidgetItem *);

private:
    Ui::MainWindow *ui;

    QVector <technology*>allTechnology;
    QVector<company *> compainies;
    QStringList str;
    company *tmp;
    int date=0;
    bool flag=false;
};

#endif // MAINWINDOW_H
