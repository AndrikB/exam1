#ifndef TECHNOLOGY_H
#define TECHNOLOGY_H
#include <QVector>
#include <QRandomGenerator>
#include <QDate>



class technology
{
public:
    technology(QString s="technology"+QString::number(number++), int cost_development=QRandomGenerator::global()->bounded(300), int time_development=QRandomGenerator::global()->bounded(9)+1, int cost_licensing=QRandomGenerator::global()->bounded(20));

    void addRequirementTechnology(technology*);
    bool isInRequirementTechnologys(technology*);//for avoid cycled requirement
    QString name(){return _name;}

    int LicensingPrice(){return costLicensing;}
    int TimeIsDeveloping(){return timeDevelopment;}
    int MoneyIsDeveloping(){return costDevelopment;}

    QVector<technology*> listTechnologysRequirement;
private:
    QString _name;
    int costDevelopment;
    int timeDevelopment;
    int costLicensing;
    static int number;

};
struct technologyStory
{
    technology* developedTechnology;
    int start, end;
    bool is_end=true;
};

#endif // TECHNOLOGY_H
