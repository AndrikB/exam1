#ifndef COMPANY_FIXEDBUDGET_H
#define COMPANY_FIXEDBUDGET_H
#include "company.h"

class company_FixedBudget:public company
{
public:
    company_FixedBudget();
    ~company_FixedBudget()override{}
    void choseNewTechnology()override;
    void start()override;
private:
    int days_end_fixed;
    int money_end_fixed;
    bool fixed=true;
};

#endif // COMPANY_FIXEDBUDGET_H
