#ifndef COMPANY_ALLTHEMSELVES_H
#define COMPANY_ALLTHEMSELVES_H
#include "company.h"

class company_AllThemSelves:public company
{
public:
    company_AllThemSelves();
    ~company_AllThemSelves()override{}
    void choseNewTechnology()override;
    void start()override;

private:


};

#endif // COMPANY_ALLTHEMSELVES_H
