#ifndef COMPANY_THINKIFISWORTHDEVELOP_H
#define COMPANY_THINKIFISWORTHDEVELOP_H
#include "company.h"

class company_ThinkIfIsWorthDevelop:public company
{
public:
    company_ThinkIfIsWorthDevelop();
    ~company_ThinkIfIsWorthDevelop()override{}
    void choseNewTechnology()override;
    void start()override;

private:
    int days;
    bool worthDevelop(technology*);
    //QDate lastDate=QDate::currentDate().addDays(+qrand()%40+20);//[20:60]
};

#endif // COMPANY_THINKIFISWORTHDEVELOP_H
