#include "company_thinkifisworthdevelop.h"

company_ThinkIfIsWorthDevelop::company_ThinkIfIsWorthDevelop()
{
    days=20+qrand()%40;
    type=3;
}

void company_ThinkIfIsWorthDevelop::choseNewTechnology()
{
    int size=ListWantedTechnology.size();
    bool b=false;
    for (int i=0;i<size;i++)
        if (listNeededToBeAdded[0]==ListWantedTechnology[i])
        {
            b=true;
            i=size;
        }

    if(worthDevelop(listNeededToBeAdded[0])||b)
    {
        addNewTechnology(listNeededToBeAdded[0]);
        listNeededToBeAdded.erase(listNeededToBeAdded.begin());
    }
    else
    {
        addNewLicense(listNeededToBeAdded[0]);
        listNeededToBeAdded.erase(listNeededToBeAdded.begin());
        choseNewTechnology();
    }

}

void company_ThinkIfIsWorthDevelop::start()
{
    listNeededToBeAdded.clear();
    create_ListNeededToBeAdded();
    if (listNeededToBeAdded.isEmpty())EndDevelop=true;
    technologies_selected=new bool[ListWantedTechnology.size()];
}

bool company_ThinkIfIsWorthDevelop::worthDevelop(technology* tech)
{
    return tech->LicensingPrice()*days>=tech->MoneyIsDeveloping();
}
