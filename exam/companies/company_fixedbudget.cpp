#include "company_fixedbudget.h"

company_FixedBudget::company_FixedBudget()
{
    days_end_fixed=qrand()%60;
    money_end_fixed=qrand()%1000;
    type=2;
}



void company_FixedBudget::choseNewTechnology()
{

    if (fixed)
    {
        int size=listNeededToBeAdded.size();
        for (int i=0;i<size;i++)
        {
            if (canAddNewTechnology(listNeededToBeAdded[i])&&
                listNeededToBeAdded[i]->MoneyIsDeveloping()>money_end_fixed&&
                listNeededToBeAdded[i]->TimeIsDeveloping()>days_end_fixed)
            {
                //add
                return;
            }
            else
            {

            }

        }
        fixed=false;
        listNeededToBeAdded.clear();
        size=ListWantedTechnology.size();

        for (int i=0;i<size;i++)
            if (!isInStory_orLicensed(ListWantedTechnology[i]))
                listNeededToBeAdded.push_back(ListWantedTechnology[i]);
    }

    if (!canAddNewTechnology(listNeededToBeAdded[0]))//if cant develop
    {
        int size=listNeededToBeAdded[0]->listTechnologysRequirement.size();
        for (int i=0; i<size;i++)
        {
            if (!isInStory_orLicensed(listNeededToBeAdded[0]->listTechnologysRequirement[i]))
            {
                addNewLicense(listNeededToBeAdded[0]->listTechnologysRequirement[i]);
            }
        }

    }

    addNewTechnology(listNeededToBeAdded[0]);
    listNeededToBeAdded.erase(listNeededToBeAdded.begin());


}

void company_FixedBudget::start()
{
    listNeededToBeAdded.clear();
    create_ListNeededToBeAdded();
    if (listNeededToBeAdded.isEmpty())EndDevelop=true;
    technologies_selected=new bool[ListWantedTechnology.size()];
}

